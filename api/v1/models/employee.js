const mongoose = require("mongoose");

const EmployeeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    gender: { type: String, required: true },
    date_of_birth: { type: String, required: true },
    date_employed: { type: String, required: true },
    department: { type: String, required: true },
    position: { type: String, required: true },
    access: {
        email: { type: String, required: true },
        password: { type: String, required: true },
    },
    createdAt: { type: String, default: Date.now },
    updatedAt: { type: String, default: Date.now }

});

module.exports = mongoose.model("Employees", EmployeeSchema);
