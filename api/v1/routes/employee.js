const express = require("express");
const bcrypt = require("bcrypt");
const router = express.Router();
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Employees = require("../models/employee");



// route requests / endpoints


// Create employee
router.post('/register', async (req, res) => {
    try {
        const employeeExists = await Employees.find({ 'access.email': req.body.access.email }).countDocuments();

        if (employeeExists !== 0) {
            res.json({ msg: "Email taken, try a different email" })
        } else {
            const hashedPswd = await bcrypt.hash(req.body.access.password, 10)
            const employee = new Employees({
                _id: new mongoose.Types.ObjectId(),
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                gender: req.body.gender,
                date_of_birth: req.body.date_of_birth,
                date_employed: req.body.date_employed,
                department: req.body.department,
                position: req.body.position,
                access: {
                    email: req.body.access.email,
                    password: hashedPswd
                }
            });

            await employee.save().then((doc) => {
                res.status(201).json(doc)
            }).catch((err) => {
                console.log(err)
                res.status(500).json({ msg: "Could not register user at this time" })
            })
        }
    } catch (error) {
        res.status(500).json({ msg: error })
    }

})

// Login Employees 
router.post('/login', async (req, res) => {
    // check if employee exists
    const employeeArr = await Employees.find({ 'access.email': req.body.email })
        .select("first_name last_name gender email date_of_birth date_employed department position access createdAt");
    const employee = employeeArr[0]

    if (employeeArr.length == 0 || employee == undefined) {
        res.status(404).json({
            msg: "Employee not found"
        })
    } else {
        try {
            // decrypt and verify employee password
            if (await bcrypt.compare(req.body.password, employee.access.password)) {
                let employeeData = {
                    id: employee._id,
                    email: employee.access.email,
                }

                // Authenticate employee with JWT
                const accessToken = await jwt.sign(employeeData, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '1h' })

                res.status(200).json({
                    token: accessToken,
                    msg: "Login suceessful"
                })

            } else {
                res.json({
                    msg: "Password not correct"
                })
            }
        } catch (err) {
            console.log(err)
        }
    }
});

// logout
router.post('/logout', authenticateToken, async (req, res) => {
    res.status(200).json({
        token: false,
        msg: "Logout suceessful"
    })
})

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    // Bearer TOKEN
    if (token == null) {
        return res.status(401).json({ "msg": "Authenication failed" })
    }

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, employee) => {
        if (err) {
            return res.status(403).json({ "msg": "Yo do not have access to this" })
        }
        req.employee = employee;
        next();
    })

}


// Read employee
router.get('/', authenticateToken, async (req, res) => {
    await Employees.find({ 'access.email': req.employee.email })
        .then((result) => {
            if (result.length !== 0) {
                let employeeData = {
                    id: result[0]._id,
                    first_name: result[0].first_name,
                    last_name: result[0].last_name,
                    gender: result[0].gender,
                    date_of_birth: result[0].date_of_birth,
                    email: result[0].access.email,
                    date_employed: result[0].date_employed,
                    department: result[0].department,
                    position: result[0].position,
                    createdAt: result[0].createdAt,
                    updatedAt: result[0].updatedAt,
                }
                res.status(200).json({
                    employee: employeeData
                })
            } else {
                res.status(404).json({
                    msg: "Not found"
                })
            }
        });
});


// Update employee data

router.patch("/:id", authenticateToken, (req, res,) => {

    const employeeId = req.params.id;
    const updateOps = {};

    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value;
    }
    Employees.updateOne({ _id: employeeId }, { $set: updateOps })
        .exec()
        .then((result) => {
            res.status(200).json({
                updatedData: updateOps,
                message: "Employees data updated"
            });
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
});


// Delete emplooyee

router.delete("/", authenticateToken, (req, res, next) => {
    const employeeId = req.body.employee_id;
    Employees.deleteOne({ _id: employeeId })
        .exec()
        .then((result) => {
            if (result.deletedCount >= 1) {
                res.status(200).json({
                    message: "Employees removed",
                    deletedCount: result.deletedCount,
                });
            } else {
                res.status(200).json({
                    message: "No employee to delete",
                    deletedCount: result.deletedCount,
                });
            }
        })
        .catch((err) => {
            res.status(500).json({
                error: err,
            });
        });
});


module.exports = router;
