require('dotenv').config()
const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const employeeAPIRoutes = require("./api/v1/routes/employee");
const jwt = require("jsonwebtoken");



mongoose.connect(process.env.DB_CONNECTION,{
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
app.use(express.json())
app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);
app.use(bodyParser.json());
app.use((req, res, next) => {
    // const allowedOrigins = process.env.ORIGINS;
    const origin = req.headers.origin;
    console.log('Accessing from: ' + origin);
    
    // if (allowedOrigins.includes(origin)) {
    //     res.setHeader('Access-Control-Allow-Origin', origin);
    // }
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With, Content-Type,Accept, Authorization"
    );
    res.header(
        "Access-Control-Allow-Methods",
        "PUT, POST, PATCH, DELETE, GET"
    );

    // if (res.method === "OPTIONS") {
        
    //     res.header(
    //         "Access-Control-Allow-Methods",
    //         "PUT, POST, PATCH, DELETE, GET"
    //     );
    //     return res.status(200).json({});
    // }
    next();
});

app.use("/api/v1/employee", employeeAPIRoutes);

app.use((req, res, next) => {
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message,
        },
    });
});

module.exports = app;
